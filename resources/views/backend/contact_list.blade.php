
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col 12">
            <form action="" method="get">
                <button type=" submit" class="btn btn-primary float-right">Search</button>
                <div class="form-group float-right mr-2">
                    <input type="text" class="form-control" id="search" value="{{$_GET['search'] ?? '' }}" name="search" placeholder="Enter keyword">
                </div>
            </form>
            </div>
        </div>

        <div class="row">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($contacts) && $contacts->count() >0)
                    @foreach($contacts as $row)
                        <tr onClick="window.location=''">
                            <th scope="row"># {{$row->id}}</th>
                            <td>{{$row->name}}</td>
                            <td>{{$row->subject}}</td>
                            <td>{{$row->email}}</td>
                            <td>{{$row->status}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>
                                <div class="btn-group float-right">
                                    <a href= "{{route('admin.contact.show', $row->id)}}" class="btn border border-secondary">View</a>
                                    <button type="button" class="btn border border-secondary">Delete</button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <p>Record not found</p>
                @endif
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-12">
                <p class="float-left mt-4">Total {{$contacts->total()}}</p>
                <div class="list-of-message float-right mt-4">
                    {!! $contacts->withQueryString()->links() !!}
                    <!-- <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#"><</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">></a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>   
@endsection
 