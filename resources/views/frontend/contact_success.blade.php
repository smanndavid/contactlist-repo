@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-8 offset-2 mb-5">
            <h1 class="border-bottom text-uppercase">Contact Submited</h1>
        </div>
        <div class="col-8 offset-2">
            <div class="text-center mt-5 pt-5">
                <h2 class="mt-5">Your message have been submit to our system!</h2>
                <h4>Our team will feedback to you soon, Thank you</h4>
            </div>

            <div class="text-center mt-5">
                <button type="button" class="btn btn-outline-dark">GO BACK TO HOME PAGE</button>
            </div>
        </div>
    </div>
    @endsection