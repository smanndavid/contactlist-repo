@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-8 offset-2 mb-5">
            <h1 class="border-bottom text-uppercase">Contact Form</h1>
        </div>
        <form class="col-8 offset-2" action="{{route('contact.pre-confirm')}}" method="post">
            @csrf
            <div class ="form-group">
                 <label for="name">Name</label>
                 <input type="text" class="form-control" name="name" id="name" placeholder="Full name" autocomplete="off" 
                        value="{{ old('name') ?? (isset($contact['name']) ? $contact['name']: '') }}">
                 @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                 @enderror
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" 
                        value="{{old('email') ?? (isset($contact['email']) ? $contact['email']: '') }}">
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="phone_number">Phone Number</label>
                    <input type="phone_number" class="form-control" name="phone_number" id="phone_number" placeholder="000 000 000" autocomplete="off" 
                        value="{{old('phone_number') ?? (isset($contact['phone_number']) ? $contact['phone_number']: '') }}">
                    @error('phone_number')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-row">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" name="subject" id="subject" placeholder="" autocomplete="off" 
                    value="{{old('subject') ?? (isset($contact['subject']) ? $contact['subject']: '') }}">
                @error('subject')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-row">
                <label for="message">Message</label>
                <textarea class="form-control" id="message" name="message" style="min-height: 128px;" autocomplete="off">
                    {{old('message') ?? (isset($contact['message']) ? $contact['message']: '') }}
                </textarea>
                @error('message')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-outline-dark float-right">Next</button>
        </form>
    </div>
    @endsection