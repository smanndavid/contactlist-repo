@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-8 offset-2 mb-5">
            <h1 class="border-bottom text-uppercase">Contact Confirmation</h1>
        </div>
        <div class="col-8 offset-2">
            <table class="table table-bordered">
            <tr>
                <th width="100px">Name</th>
                <td>
                    @isset($contact['name'])
                        {{ $contact['name'] }}
                    @endisset
                </td>
            </tr>
            <tr>
                <th>Email</th>
                <td>
                    @isset($contact['email'])
                        {{ $contact['email'] }}
                    @endisset
                </td>
            </tr>
            <tr>
                <th>Phone Number</th>
                <td>
                @isset($contact['phone_number'])
                    {{ $contact['phone_number'] }}
                @endisset
                </td>
            </tr>
            <tr>
                <th>Subject</th>
                <td>
                @isset($contact['subject'])
                    {{ $contact['subject'] }}
                @endisset
                </td>
            </tr>
            <tr>
                <th>Message</th>
                <td>
                @isset($contact['message'])
                    {{ $contact['message'] }}
                @endisset
                </td>
            </tr>
            </table>
            <div class="mt-2">
                 <a href="{{route('contact.index')}}" type="button" class="btn btn-outline-dark float-left">Back</a>
                 <form method="post" action="{{route('contact.store')}}">
                    @csrf
                    <input type="submit" class="btn btn-outline-dark float-right" name="submit" value="Submit"/>
                 </form>
            </div>
        </div>
    </div>
    @endsection