<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Contacts;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){

        //check and reload data from session
        $contact = null;

        if(session()->has('tmp_contact')){
            $contact = session('tmp_contact');
        }
        return view('frontend.contact', compact('contact'));
    }

    public function preConfirm(Request $request){
        $request->validate([
            'name' =>'required|max:50',
            'email' =>'required|email|max:100',
            'phone_number' =>'required|max:10',
            'subject' =>'required|max:100',
            'message' =>'required'
        ]);
        
        $contact = $request->only(['name', 'email', 'phone_number', 'subject', 'message']);
        //store data to session()
        session(['tmp_contact' => $contact]);

        //redirect to confirm view
        return redirect()->route('contact.confirm');
    }

    public function store(){
       
        //check data in session
        if (session()->has('tmp_contact')) {

            // store data to database
            $contact = session('tmp_contact');
            $contact['updated-by'] = 0;
            $row = Contacts::create($contact);

            if ($row) {
                //clear data from session
                session()->forget('tmp_contact');

                //redirect to success view
                return redirect()->route('contact.success');
            }
        }
        abort(404);
    }


    public function confirm(){

        //check data from session
        $contact = null;
        if(session()->has('tmp_contact')){
            $contact = session('tmp_contact');
        }
        return view('frontend.contact_confirm', compact('contact'));
    }

    public function success(){
        return view('frontend.contact_success');
    }

    



}
