<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Contacts;
use Illuminate\Http\Request;


class ContactController extends Controller
{
    public function index(Request $request)
    {
        $key = $request->get('search', null);
        if ($key){
            $contacts= Contacts::where("name", "like","%$key%")
                ->orWhere("email", "like", "%$key%")
                ->orWhere("phone_number", "like", "%$key%")
                ->orWhere("subject", "like", "%$key%")
                ->paginate(10);
        }else{
            $contacts= Contacts::paginate(10);
        }
        return view('backend.contact_list', compact('contacts')); 
    }

    public function show($contactId)
    {
        $contact = Contacts::whereId($contactId)->firstOrFail();
        return view('backend.contact_detail', compact('contact'));
    }

    public function put(Request $request, $contactID)
    {
        $status = $request->status;
        $contact = Contacts::find($contactID);

        if($contact){
            $contact->status =$status;
            $contact->save();
        }

        return redirect()->back();
    }
}
